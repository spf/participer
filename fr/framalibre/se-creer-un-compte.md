# Se créer un compte Framalibre

Si vous voulez tirer amplement parti de cet annuaire, nous vous recommandons fortement de vous créer un compte...

## Comment se créer un compte ?

![capture se créer un compte](images/framalibre02.png "Page de création de compte")
1. Cliquez sur "Créer un compte" en haut à droite.

2. Entrez un nom d'utilisateur/utilisatrice (les espaces sont permis !) et une adresse email valide (elle vous servira de contact régulièrement dans votre utilisation de l'annuaire).
N'oubliez pas de déjouer le CAPTCHA pour montrer que vous n'êtes pas un robot (oui, c'est tellement facile qu'on pourrait avoir des doutes, mais non... y'a pas de piège !)

3. Ouvrez votre boite email pour cliquer sur le lien d'activation dans les 24h suivant vote demande (si vous ne le trouvez pas dans la boite de réception, vérifiez le dossier des indésirables ;) !)

4. Remplissez le formulaire de présentation et cliquez sur "Enregistrer"

*À noter : seuls les champs de votre mot de passe sont obligatoires, le reste, c'est juste si vous souhaitez agrémenter votre profil ^^ !*

## Pour donner des étoiles !

![capture donner des étoiles](images/framalibre03.png "Exemple : la notice de Framaphère")

Sur chacune des notices, vous pouvez attribuer de 1 à 5 étoiles à la ressource concernée.
Ces étoiles concernent la ressource (le logiciel, le livre, le site web) et non la qualité de la notice.
Ce système de notation permet, à terme, de faire remonter les ressources les plus pertinentes et utilisées dans les différent modules, et de donner un aperçu des avis populaires aux personnes qui viendraient découvrir une nouvelle ressource.

## Pour créer votre registre !

Votre registre contiendra toutes vos notices favorites, c'est à dire les notices dont vous avez cliqué l'icône cœur.

![capture consigner dans son registre](images/framalibre04.png "Exemple : consigner « Avant de Dormir »")

C'est l'occasion, pour vous, de retrouver facilement un lien direct vers les notices (et donc les ressources) qui vous intéressent le plus. Que ce soit parce que vous suivez la création et les modifications de cette notice, ou parce que vous utilisez/appréciez la ressource concernée, vous la retrouverez alors dans la page Mon Registre.

Cette page vous permet de trier aisément l'ensemble de vos notices favorites, que ce soit par Type, catégorie ou auteur. Bien entendu vous avez la possibilité de retirer la notice de vos favoris (et donc de votre registre).

Avec une future API, il devrait être possible d'afficher tout ou partie de son registre vers un autre site web ;)

![Capture Mon registre](images/framalibre05.png "Un registre qui commence à peine à se remplir...")

En attendant, vous pouvez d'ores et déjà rendre votre registre public :

  * Rendez-vous dans `Moncompte / Modifier`
  * Cochez « Rendre mon registre public »
  * Cliquez sur « Enregistrer » tout en bas.
  * Notez que l'adresse publique de votre register sera `framalibre.org/monpseudo/favoris`

## Pour rédiger une chronique !

Une chronique est un texte personnel lié à une notice (et donc à la ressource qu'elle présente).

Chacun·e est libre de rédiger les chroniques qu'il ou elle souhaite, mais les usages et intérêts immédiats que nous y voyons sont :

  * Un tutoriel lié à la ressource (utilisation logicielle, matériel, etc.)
  * Un avis professionnel ou amateur sur la ressource (test, critique culturelle, impressions)
  * Des impressions ou réflexions quant à la ressource
  * Toute autre libre expression, dans le respect de nos [conditions générales d'utilisations](https://framasoft.org/nav/html/cgu.html) (et donc de la Loi)

![capture page d'une notice, compte connecté](images/framalibre06.png "Soumettre une mise à jour ou rédiger une chronique...")

Pour rédiger une chronique, c'est simple :
    1. connectez-vous à votre compte Framalibre
    2. allez sur la notice de la ressource concernée
    3. cliquez sur "Rédiger une chronique sur cette notice"


## Pour créer / modifier une notice !

C'est le but principal : contribuer et donner des apports à l'annuaire. Mais ça mérite [son propre article](creer-modifier-une-notice.html), non ?
