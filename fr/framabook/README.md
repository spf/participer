# Participer à <b class="violet">Frama</b><b class="rouge">book</b>


## Relire les <b class="violet">Frama</b><b class="rouge">book</b>

Pour être une collection de livres libres, nous n'en avons pas moins pour objectif la meilleure qualité éditoriale possible. Pour cela nous avons toujours besoin de relecteurs volontaires.

<img class="img-responsive center-block" src="images/apotypogege.png" alt="Apo typo gege" />

### Auto-testez vous ;)

<div class="well">Vous souhaitez contribuer activement au projet <b class="violet">Frama</b><b class="rouge">book</b> et participer à un grand projet culturel libre&nbsp;? Vous détestez les erreurs de ponctuation, les majuscules inappropriées, les coquilles et les fautes d'orthographe&nbsp;? Les règles de la langue française et de la typographie n'ont (presque) plus de secret pour vous? Voilà une très bonne manière de contribuer au Libre&nbsp;! Vous trouverez sur cette page les explications et procédures concernant la phase de relecture des livres de la collection.
</div>

<p>En téléchargeant <a href="https://framabook.org/wordpress/wp-content/uploads/2017/12/orthotypotest.pdf">ce fichier PDF de 3 pages</a>, vous pourrez aussi faire un petit test (page après page) de manière à situer vos compétences. L'objectif n'est pas d'avoir tout juste, mais de mieux comprendre les enjeux de la relecture et évaluer votre expérience en la matière.</p>

<div class="row">
	<p class="col-md-6 col-md-offset-3 text-center">
    <a class="btn btn-primary btn-lg btn-block" href="https://framabook.org/wordpress/wp-content/uploads/2017/12/orthotypotest.pdf"><i class="fa fa-fw fa-file-pdf-o"></i> Télécharger le .pdf<br>pour s'auto-tester.</a>
	</p>
</div>


<p>Enfin, merci de prendre quelques instants pour lire les quelques lignes suivantes avant de vous signaler.</p>



### Quelles sont les procédures&nbsp;?

<p>Le projet <b class="violet">Frama</b><b class="rouge">book</b> dispose de plusieurs moyens d'édition. Cela va de l'éditeur en ligne collaboratif, à l'échange de fichier <code>markdown</code>, <code>.odt</code> ou <code>.TeX</code>.</p>

<ul>
 	<li>Nous appelons <strong>phase 1</strong> de relecture le travail éditorial mené tout au long des projets avec les auteurs.</li>
 	<li>Nous appelons <strong>phase 2</strong> la relecture du manuscrit final.</li>
</ul>

<p>Si la phase 1 peut s'accommoder de divers formats de fichiers, la seconde phase doit se faire quasi-séparément sur la base des fichiers finaux qui serviront à la publication, c'est à dire le fichier PDF qui servira à l'impression des livres «&nbsp;papier&nbsp;» (issu d'une maquette LaTeX, ou même parfois, plus en amont, d'un document en Markdown), et le fichier HTML qui servira (lorsqu'il est possible de le réaliser) à la publication HTML et E-pub. Dans tous les cas, le fichier PDF sert à la fois pour l'impression et la mise en téléchargement libre, c'est à dire qu'il sert de référent.</p>

<p>Compte-tenu des usages différents des deux formats (PDF et HTML), leur relecture ne se passe pas de la même manière. Par exemple les besoins d'ordre typographique seront différents, idem pour l'encodage. Par contre, évidemment, les corrections orthographiques obéiront dans les deux cas à la même méthode.</p>

### Y a-t-il beaucoup de relecteurs&nbsp;?

<p>Il va sans dire que pour une opération aussi délicate, le nombre de relecteurs n'est pas un gage d'efficacité. En effet, mieux vaut un bon relecteur qui remonte un fichier correctement annoté / corrigé, que 10 relecteurs qui remontent 10 fichiers survolés, voire avec des corrections inappropriées.</p>

<p>L'analogie avec la contribution à l'élaboration d'un programme, sous forme de <em>commits</em>, a ses limites&nbsp;: ici, le travail est essentiellement d'ordre éditorial. Il se peut même que, parfois, la correction à l'aide d'un crayon de papier et d'une gomme sur la terrasse d'un café soit plus efficace que l'utilisation d'un clavier et d'un écran&hellip; même s'il est préférable de remonter les corrections rapidement sans devoir passer par les services postaux (un scanner peut s'avérer utile).</p>

### Ce qu'il faut relire

<p><strong>Un bon relecteur sera attentif à deux choses&nbsp;:</strong></p>
<ol>
 	<li>L'orthographe et le style, pour lesquels il effectue des corrections directes ou des propositions selon les cas. Il sait ne pas faire «&nbsp;trop&nbsp;» de propositions&nbsp;: en tant que relecteur, il prend aussi la responsabilité du texte, et par conséquent est habilité à corriger directement le manuscrit que l'auteur a confié, dans la mesure où il n'en change pas le sens. Commentaires et propositions trouvent leur place lors de la phase 1, lorsque le livre est en cours de rédaction. Ils sont logiquement bien moindres lors de la phase 2, qui est censée intégrer les remarques (l'une des caractéristiques du projet <b class="violet">Frama</b><b class="rouge">book</b> est de reposer, autant que possible, sur un travail collaboratif).</li>
 	<li>En ce qui concerne la typographie, l'exercice est tout aussi difficile&nbsp;! Dans la majorité des cas, la référence en matière de typographie ne se trouve pas sur Internet (la plupart des pages consacrées sont incomplètes, voire inexactes). Un livre fait référence&nbsp;: le <em>Lexique des règles typographiques en usage à l'Imprimerie nationale</em> (<a href="https://fr.wikipedia.org/wiki/R%C3%A9f%C3%A9rence:Lexique_des_r%C3%A8gles_typographiques_en_usage_%C3%A0_l%27Imprimerie_nationale">Ref.</a>), en vente chez les bons libraires. En cas de doute, nous nous y référerons, bien qu'il ne constitue pas la recette ultime (par exemple, nous choisissons une présentation bibliographique différente, plus en accord avec les normes ISO en vigueur).</li>
</ol>

<p>Enfin, toujours à propos de la typographie, le <a title="Lacroux" href="http://www.orthotypographie.fr/">site-livre de Jean-Pierre Lacroux</a> peut s'avérer d'une grande utilité concernant les points de détail.</p>



### Quels logiciels sont utilisés&nbsp;?


<ul>
 	<li>LibreOffice (et tout autre système capable de travailler des formats .odt),</li>
 	<li>LaTeX + BibLateX (les relecteurs ne sont pas censés devoir maîtriser LaTeX),</li>
 	<li>Des éditeurs de texte</li>
 	<li>Des logiciels permettant d'annoter des PDF (comme Xournal)</li>
 	<li>autres solutions (la collection est en perpétuelle amélioration).</li>
</ul>

<p>Pour la relecture directe sur PDF (passage souvent obligé en phase 2 de la relecture), deux solutions sont possibles&nbsp;:</p>
<ul>
 	<li>imprimer chez soi le texte à relire, l'annoter (clairement), scanner les pages et les renvoyer (peu écologique mais parfois bien efficace),</li>
 	<li>utiliser un logiciel d'annotation de PDF, utilisant un calque que l'on fusionne ensuite sur le PDF original pour renvoyer le fichier ainsi annoté.</li>
</ul>

<p>Concernant la seconde solution, vous pouvez utiliser plusieurs logiciels libres sur votre système d’exploitation. Voyez ces deux petits tutoriels&nbsp;:</p>

<ul>
 	<li><a href="images/UtilisationdeJarnal.pdf">Utilisation de Jarnal</a></li>
 	<li><a href="images/xournaletflpsed.pdf">Utilisation des logiciels Xournal et lfpsed</a>.</li>
</ul>

<p>Conseil&nbsp;: faites des essais avant d'annoter tout un document&hellip;</p>


### Rejoindre le groupe

Vous êtes décidé·e&nbsp;? Parfait&nbsp;!

Il vous suffit d'envoyer un courriel par l'intermédiaire du <a href="https://contact.framasoft.org/#Framabook">formulaire de contact</a>.

Une petite information, toutefois&nbsp;: nous communiquons grâce à une liste de diffusion (Sympa) et stockons nos fichiers de travail sur un serveur dans les nuages. Il sera important de se conformer aux usages (classiques) qui vous seront explicités dès votre intronisation&nbsp;:)


## Proposer votre ouvrage à <b class="violet">Frama</b><b class="rouge">book</b>

<div class="alert alert-danger">
<p> Attention&nbsp;: <b class="violet">Frama</b><b class="rouge">book</b> ne publie pas par défaut tout ouvrage sous licence libre. C'est une maison d'édition, avec un comité de lecture composé de bénévoles passionné·e·s, qui choisissent les ouvrages selon leur temps et leurs énergies disponibles d'une part, et selon la pertinence de l'ouvrage dans la collection de l'autre.<p>
</div>

<p>Néanmoins, si l'aventure vous intéresse, il vous faut simplement&nbsp;:</p>
<ul>
	<li>Aller lire <a href="https://Framabook.org/politique-editoriale-de-Framabook/">notre politique éditoriale</a> (détaillant, entre autres, notre choix de refuser systématiquement les licences «&nbsp;presque libres&nbsp;», par exemple les -NC ou -ND chez <i lang="en">Creative Commons</i>) ;</li>
	<li>Aller lire nos <a href="https://Framabook.org/section-des-auteurs-et-consignes/">consignes aux auteur·trice·s</a> ;</li>
	<li>Nous présenter votre ouvrage (extrait, synopsis et/ou tapuscrit) avec un résumé et vos intentions via <a href="https://contact.framasoft.org/#Framabook">notre formulaire de contact</a></li>
</ul>
