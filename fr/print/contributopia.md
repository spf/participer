# Outils de communication <i lang ="en">print</i> pour <b class="violet">Contributopia</b>

**Nous avons différents formats possibles :**

  * Les flyers (présentation courte) ;
  * Les stickers (autocollants) ;
  * Les affiches (décoration) ;
  * Les kakemono (ou Roll-up, pour des stands, etc.).


## Flyer
### Le monde des services de <b class="vert">Contributopia</b>

**Format**

  * A6 (10,5 x 14,8 cm), recto-verso, couleurs
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: Lato

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregistrer la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="images/flyercontributopia-recto-paysage-v2.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/contributopia-flyer-recto-paysage-v2.png" /></a>
        <p class="text-center">Recto</p>
    </div>
    <div class="col-sm-6">
        <a href="images/flyercontributopia-verso-paysage.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/contributopia-flyer-verso-v1-services-paysage.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>

## Stickers
### Le monde des services de <b class="vert">Contributopia</b>

**Format**

  * Rond 6 cm, recto couleurs, verso N&B
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Police d'écriture utilisée&nbsp;: Sans-serif.

**Le rendu & les sources**

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choisissez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="images/Contributopia-services_Sticker_rond.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/Contributopia-services_Sticker_rond.png" /></a>
        <p class="text-center">Recto</i></p>
    </div>
    <div class="col-sm-6">
        <a href="images/Framasoft_Sticker_back02.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/Framasoft_Sticker_back02.png" /></a>
        <p class="text-center"><i lang="en">Verso (facultatif)</p>
    </div>
</div>

## Affiches

### Les mondes de <b class="vert">Contributopia</b>
**Format**

  * A1 paysage, 150 DPI, couleurs
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Police d'écriture utilisée&nbsp;: Sans-serif.

**Le rendu & les sources**
<div class="well">
	<strong>ATTENTION&nbsp;: fichiers volumineux !</strong>
	<p>Les images affichées ici sont des petits formats pour l'affichage web, <b>ils ne peuvent pas servir pour de l'impression papier.</b></p>
	<p>Les véritables fichiers sont disponibles depuis les boutons en dessous de l'image.</p>
	<p>Pour enregistrer ces gros fichiers, cliquez sur ces boutons.</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Version web allégée de l'image, ne pas imprimer" alt=""
            src="images/Contributopia-Accueil-web.png" />
        <p class="text-center">Recto</i></p>
    </div>
</div>

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Accueil-150dpi-5mmdebord.pdf" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></p>
      <p>.pdf - 34,8 Mo</p>
      <p><b>Impression</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Accueil.png" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-picture-o" aria-hidden="true"></p>
      <p>.png - 16,1 Mo</p>
      <p><b>Image</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Accueil.svg" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></p>
      <p>.svg - 36,7 Mo</p>
      <p><b>Source</b></p>
    </a>
  </div>
</div>

### <b class="vert">Contributopia</b>&nbsp;: les services
**Format**

  * A1 portrait, 150 DPI, couleurs
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Polices d'écritures utilisées&nbsp;: Sans-serif, PT sans caption.

**Le rendu & les sources**
<div class="well">
	<strong>ATTENTION&nbsp;: fichiers volumineux !</strong>
	<p>Les images affichées ici sont des petits formats pour l'affichage web, <b>ils ne peuvent pas servir pour de l'impression papier.</b></p>
	<p>Les véritables fichiers sont disponibles depuis les boutons en dessous de l'image.</p>
	<p>Pour enregistrer ces gros fichiers, cliquez sur ces boutons.</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Version web allégée de l'image, ne pas imprimer" alt=""
            src="images/Contributopia-Services-web.png" />
        <p class="text-center">Recto</i></p>
    </div>
</div>

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Services-150dpi-5mmdebord.pdf" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></p>
      <p>.pdf - 39,7 Mo</p>
      <p><b>Impression</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Services.png" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-picture-o" aria-hidden="true"></p>
      <p>.png - 11,2 Mo</p>
      <p><b>Image</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Services.svg" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></p>
      <p>.svg - 37,9 Mo</p>
      <p><b>Source</b></p>
    </a>
  </div>
</div>

### <b class="vert">Contributopia</b>&nbsp;: l'essaimage
**Format**

  * A1 portrait, 150 DPI, couleurs
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Polices d'écritures utilisées&nbsp;: Sans-serif, PT sans caption.

**Le rendu & les sources**
<div class="well">
	<strong>ATTENTION&nbsp;: fichiers volumineux !</strong>
	<p>Les images affichées ici sont des petits formats pour l'affichage web, <b>ils ne peuvent pas servir pour de l'impression papier.</b></p>
	<p>Les véritables fichiers sont disponibles depuis les boutons en dessous de l'image.</p>
	<p>Pour enregistrer ces gros fichiers, cliquez sur ces boutons.</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Version web allégée de l'image, ne pas imprimer" alt=""
            src="images/Contributopia-Essaimage-web.png" />
        <p class="text-center">Recto</i></p>
    </div>
</div>

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Essaimage-150dpi-5mmdebord.pdf" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></p>
      <p>.pdf - 42,0 Mo</p>
      <p><b>Impression</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Essaimage.png" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-picture-o" aria-hidden="true"></p>
      <p>.png - 11,3 Mo</p>
      <p><b>Image</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-Essaimage.svg" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></p>
      <p>.svg - 37,7 Mo</p>
      <p><b>Source</b></p>
    </a>
  </div>
</div>

### <b class="vert">Contributopia</b>&nbsp;: l'éducation populaire
**Format**

  * A1 portrait, 150 DPI, couleurs
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Polices d'écritures utilisées&nbsp;: Sans-serif, PT sans caption.

**Le rendu & les sources**
<div class="well">
	<strong>ATTENTION&nbsp;: fichiers volumineux !</strong>
	<p>Les images affichées ici sont des petits formats pour l'affichage web, <b>ils ne peuvent pas servir pour de l'impression papier.</b></p>
	<p>Les véritables fichiers sont disponibles depuis les boutons en dessous de l'image.</p>
	<p>Pour enregistrer ces gros fichiers, cliquez sur ces boutons.</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Version web allégée de l'image, ne pas imprimer" alt=""
            src="images/Contributopia-EducPop-web.png" />
        <p class="text-center">Recto</i></p>
    </div>
</div>

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-EducPop-150dpi-5mmdebord.pdf" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></p>
      <p>.pdf - 42,9 Mo</p>
      <p><b>Impression</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-EducPop.png" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-picture-o" aria-hidden="true"></p>
      <p>.png - 11,8 Mo</p>
      <p><b>Image</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="images/Contributopia-EducPop.svg" download class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></p>
      <p>.svg - 39,9 Mo</p>
      <p><b>Source</b></p>
    </a>
  </div>
</div>
