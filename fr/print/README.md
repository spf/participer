# Flyers, affiches et Stickers


## Nos outils prints et leurs sources

Vous trouverez sur cette page les informations, rendus et fichiers sources pour reproduire et/ou adapter nos outils de communication <i lang="en">print</i> autour des projets <b class="violet">Frama</b><b class="orange">soft</b>, <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b> et <b class="violet">Contributopia</b>.

Ces données sont partagées afin que vous puissiez vous-même produire (et modifier, adapter&hellip;) ces documents, en toute autonomie.

Nous les avons classés selon le sujet :

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="framasoft.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-pencil-square-o" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="orange">soft</b></p>
      <p><b>Print</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="degooglisons.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-shield" aria-hidden="true"></p>
      <p><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></p>
      <p><b>Print</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="contributopia.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-share-alt" aria-hidden="true"></p>
      <p><b class="violet">Contributopia</b></p>
      <p><b>Print</b></p>
    </a>
  </div>
</div>


## Recevoir des documents prints

Le plus simple, c'est de commander directement les documents prints que nous éditons, directement chez EnVenteLibre.

<div class="clearfix">
  <div class="col-md-6 col-md-offset-3">
    <a href="https://enventelibre.org/40-framasoft" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-shopping-cart" aria-hidden="true"></p>
      <p><b>EnVente Libre</b></p>
      <p><b>section <b class="violet">Frama</b><b class="orange">soft</b></b></p>
  </div>
</div>

****

Il nous arrive d'envoyer nous-même des documents print pour soutenir des *événements, rencontres, initiatives*, etc. qui veulent diffuser un message libriste et qui n'ont pas les moyens de les commander.

Néanmoins, l'impression et l'envoi de ces documents étant financés par les dons que nous recevons et pris sur le temps de nos salarié·e·s, **ces envois sont exceptionnels et se font uniquement après une demande motivée de la part d'un collectif ou d'une association** (les demandes de particulier·e·s seront automatiquement rejetées).

Si vous désirez recevoir des flyers, etc. merci de <a href="https://contact.framasoft.org">nous faire une demande ici</a> en précisant&nbsp;:
  * Pour quel collectif et quel événement/usage&nbsp;;
  * Pourquoi vous demandez un envoi gracieux (cela restera entre nous, promis&nbsp;^^&nbsp;!)&nbsp;;
  * Les délais souhaités (mieux vaut prévoir à l'avance)&nbsp;;
  * Une estimation (même grossière) du nombre de personnes à qui vous pensez les distribuer.
  
**Nous ne pourrons pas répondre favorablement à toute demande, mais nous ferons de notre mieux pour voir cela avec vous&nbsp;!**













