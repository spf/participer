# Participer à Framasoft


## Les sites principaux

  <div class="col-xs-12">
    <a href="https://framacolibri.org" class="btn btn-lg btn-block btn-default">
      <p><b>Le forum des Framacolibris<br>
      <i>— L'endroit où on échange, s'organise, demande et répond.</i></b></p>
    </a>
  </div>
  <div class="col-xs-12">
    <a href="https://framagit.org/framasoft" class="btn btn-lg btn-block btn-default">
      <p><b>Les dépôts git de Framasoft<br>
      <i>— Où nous publions les codes que nous produisons, à utiliser et améliorer.</i></b></p>
    </a>
  </div>

## Les projets communautaires

<!--
  La liste est classés par ordre alphabetique de Framaservice

  /!\ Ne pas faire pointer vers la racine du dossier
      mais bien sur "index.html"
-->
<div class="clearfix">
  <div class="col-md-3 col-sm-6">
    <a href="framablog/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-pencil" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="rouge">blog</b></p>
      <p><b>Rédaction</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framabook/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-book" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="rouge">book</b></p>
      <p><b>Édition</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framacode/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-code" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="jaune">code</b></p>
      <p><b>Développement</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framakey/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-usb" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="bleu">key</b></p>
      <p><b>Clé usb</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framalang/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-language" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="rouge">lang</b></p>
      <p><b>Traduction</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framalibre/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-linux" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="bleu">libre</b></p>
      <p><b>Annuaire</b></p>
    </a>
  </div>
</div>

## Libérer son entourage !

<div class="clearfix">
  <div class="col-md-4 col-sm-6">
    <a href="print/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-paint-brush" aria-hidden="true"></p>
      <p><b class="violet">Visuels</b> et <b class="orange">sources</b></p>
      <p><b>Print</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="essentiels/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-link" aria-hidden="true"></p>
      <p><b class="violet">Liens</b> <b class="orange">essentiels</b></p>
      <p><b>Nos sites</b></p>
    </a>
  </div>
  <div class="col-md-4 col-sm-6">
    <a href="conferences/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-slideshare" aria-hidden="true"></p>
      <p><b class="violet">Présentations</b> et <b class="orange">confs</b></p>
      <p><b>Voir et faire</b></p>
    </a>
  </div>
</div>

## Nos visuels

<div class="well">
<p>Les boutons présentés ci-dessous vous mènent vers des sites extérieurs à cette documentation. Si vous désirez néanmoins conserver ce site dans un onglet, faites un <strong>clic droit + «&nbsp;ouvrir dans un nouvel onglet&nbsp;»</strong></p>
</div>

<div class="clearfix">
	<div class="col-md-4 col-sm-6">
		<a href="https://www.peppercarrot.com/en/static6/sources&page=other" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-share-alt" aria-hidden="true"></p>
		<p><b class="vert">Contributopia</b></p>
		<p><b>Illustrations de David Revoy</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://degooglisons-internet.org/medias" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-shield" aria-hidden="true"></p>
		<p><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></p>
		<p><b>Page médias</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://framasoft.org/nav/html/graphics.html" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-pencil-square-o" aria-hidden="true"></p>
		<p><b class="violet">Frama</b><b class="orange">soft</b></p>
		<p><b>Charte Graphique</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://commons.wikimedia.org/wiki/File:Framasoft_Logo.svg" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-file-image-o" aria-hidden="true"></p>
		<p><b class="violet">Frama</b><b class="orange">soft</b></p>
		<p><b>Logo</b></p>
		</a>
	</div>
</div>
