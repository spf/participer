# Contribuer facilement sur Framagit

Pour participer à la traduction d'un logiciel ou bien pour contribuer à
sa documentation, il n'est pas nécessaire d'en passer par la ligne de commande.

Vous pouvez travailler sur [nos dépôts git](https://framagit.org/framasoft/)
entièrement en ligne.

Voici la méthode de travail pour, par exemple, corriger un fichier de langue
de notre site [Dégooglisons Internet](https://degooglisons-internet.org).

## Étape 1 : se rendre sur la page du projet et le copier

Créez d’abord un compte sur Framagit.

Il faut commencer par *forker* le projet. C’est-à-dire faire une copie
sur laquelle on fera les modifications.

**Les modifications sont à faire sur la copie (ou fork), pas sur le projet initial**

 *  Sur [la page du projet](https://framagit.org/framasoft/degooglisons),
    cliquez sur l’icône *Fork* ![](images/icon_fork.png)
 *  Cliquez sur votre avatar ou pseudo ![](images/avatar_fork.png)

Vous avez désormais un copie du projet dans votre répertoire :

![](images/fork_min.png)

![](images/fork_min.gif)


## Étape 2 : modifications de fichiers

Commencez par cliquer sur l'icône **Files** dans la barre de navigation
(à gauche) pour afficher les dossiers et fichiers du projet.
Pour le projet *Degooglisons*, les fichiers à modifier se situent
dans le dossier ``degooglisons/i18n/``.
Généralement, les fichiers destinés à la traduction se situent dans
un dossier ``i18n``, ou ``l10n`` ou encore ``locale``.
Par convention, le nom du fichier contient les initiales de la langue :

 *  data.**en**.php pour l'anglais
 *  data.**es**.php pour l'espagnol
 *  data.**it**.php pour l'italien
 *  etc

Pour modifier un fichier, il faut cliquer dessus, puis sur ``EDIT``

![icon edit](images/icon_edit.png)

<p class="alert alert-warning">
    <b class="label label-primary">Attention</b><br>
    Pour de la traduction, il faut changer la valeur (à droite de <code>=</code>)
    pas la clé.<br>
    Par exemple : <code>$t['home']['enje']['btn']   = 'Quels sont les enjeux ?';</code>
    devient <code>$t['home']['enje']['btn']   = 'What are the stakes?';</code>.
    Il ne faut pas modifier <code>$t['home']['enje']['btn']</code>.
</p>

Je veux corriger la faute ``"Quel sont les enjeux ?"`` dans le fichier
``degooglisons/i18n/data.fr.php`` :

 *  je clique sur le fichier pour l'afficher
 *  je clique sur ``EDIT`` pour le modifier
 *  je vais à la ligne concernée (ici ``#117``)
 *  je fais ma modification (1)
 *  j'écris un message d'explication du *commit* (2)
 *  je clique sur ``COMMIT CHANGES`` pour que ma modification soit enregistrée (3)

![](images/commit_annotations_min.png)

Si vous avez d'autres modifications à faire (sur le même fichier ou sur
un autre), reprenez cette étape.

## Étape 3 : Envoyer la modification

Maintenant que le fichier est modifié sur **mon compte**, je dois
l'envoyer sur le compte Framasoft (si vous voulez faire plusieurs
modifications, attendez de toutes les avoir faites pour envoyer votre *MR*).
Pour cela :

 *  je clique sur le bouton ``MERGE REQUESTS``
 *  je clique sur le bouton ``NEW MERGE REQUEST`` pour récupérer ma *Merge request*
 *  dans le cadre ``Source branch`` je clique sur ``Select branch``
    puis sur la branche sur laquelle j'ai fait les modifications.
    Ici ``master`` (ça pourrait être aussi ``develop`` par exemple ;
    tout dépend des consignes données par les administrateurs du projet)

![](images/compare_MR_min.png)

Je peux désormais voir ma modification.

 *  je clique sur ``COMPARE BRANCHES``

![](images/envoi_MR_min.png)

On peut voir sur la ligne ``From spf/degooglisons:master into framasoft/degooglisons:master``
que la modification a été faite **depuis** la branche ``master`` de
**mon** dépôt (``spf/degooglisons:master``) **vers** la branche
``master`` du dépôt Framasoft (``framasoft/degooglisons:master``)

Si le titre (ici ``Correction d'un accord``) ne nécessite pas de plus
amples informations, vous n'êtes pas obligé de mettre une description.

Il reste à cliquer sur ``SUBMIT NEW MERGE REQUEST`` pour envoyer les
modifications.

![](images/discution_MR_annot_min.png)

Votre modification a été envoyée. Elle sera soit intégrée (*mergée*)
soit discutée.

Tant que votre *MR* n'est pas *mergée* (acceptée) vous pouvez continuer
à faire des *commits* (modifications) dessus.
Pour cela, reprenez l'étape 2.

Vous recevrez un mail si votre *MR* est acceptée.
Vous pouvez aussi aller sur la page du projet -> ``Merge Requests``.
Si votre *MR* est dans l'onglet ``Merged`` c'est qu'elle a été acceptée
(sinon, elle se trouvera dans ``Open``).

## Étape 4 : Suppression de VOTRE version

<p class="alert alert-warning">
    <b class="label label-primary">ATTENTION</b>
    Ne supprimez pas votre version tant qu'elle n'a pas été intégrée !
</p>

 *  retournez sur votre version du projet en cliquant sur le logo
    **Framasoft**, puis sur votre copie du projet (ici ``spf/degooglisons``)
 *  cliquez sur l'engrenage (sous votre avatar, tout en haut à droite),
    puis ``Edit Project``
 *  allez en bas de la page et cliquez sur ``REMOVE PROJECT`` puis
    confirmez en entrant le nom du projet que vous souhaitez supprimer
    (ici ``degooglisons``).

![](images/delete_repo.gif)

Pour faire une nouvelle modification, reprenez l'étape 1.
