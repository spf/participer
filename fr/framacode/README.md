# Framacode

## Logiciels

<div class="well">
    <p>
        Avant tout, sachez que <strong><b class="violet">Frama</b><b class="orange">soft</b>
        ne fait pas de développement</strong>. À quelques exceptions près,
        nous ne faisons qu’adapter légèrement des logiciels existants pour
        les proposer au public.
    </p>
    <p>
        Pour participer, il est donc important de savoir si vos
        contributions concernent réellement les adaptations réalisées
        pour les projets <b class="violet">Frama</b><b class="orange">soft</b>
        (ci-dessous nos dépôts sont indiqués d’un
        <i class="fa fa-fw fa-lg fa-git" aria-hidden="true"></i>
        <span class="sr-only">git</span>) ou bien les logiciels originaux
        (<i class="fa fa-fw fa-lg fa-code-fork" aria-hidden="true"></i>
        <span class="sr-only">source</span>).
    </p>
    <p>
        Il sera plus efficace de signaler un bug ou proposer des
        améliorations au code, si vous le faites depuis les sites de
        développements respectifs.
    </p>
</div>

<div id="framacode-list"></div>

## Documentations

Plusieurs de nos sites sont des documentations écrites en markdown et
structurées avec Gitbook.

Pour enrichir ces documentations, ça se passe aussi [en ligne sur Framagit
pour les petites retouches](git-en-ligne.html) mais il vous faudra
probablement en passer par la ligne de commande pour des apports
plus conséquents.
Ne vous inquiétez pas [on vous guide](gitbook.html) :)

<table class="table table-bordered" role="presentation">
    <tbody>
        <tr>
            <td>
                <i class="fa fa-fw fa-lg fa-graduation-cap" aria-hidden="true"></i>&nbsp;
                <a href="https://docs.framasoft.org/fr/" class="btn btn-link">
                    <b class="jaune">Docs</b>
                </a>
                <a href="https://framagit.org/framasoft/docs/" class="btn btn-link pull-right">
                    <i class="fa fa-fw fa-lg fa-git" aria-hidden="true"></i>
                    <span class="sr-only">git</span>
                </a>
            </td>
            <td>
                pour accompagner les <b>utilisateur⋅rice⋅s</b><br>
                des services proposés par Framasoft
            </td>
        </tr>
        <tr>
            <td>
                <i class="fa fa-fw fa-lg fa-cloud" aria-hidden="true"></i>&nbsp;
                <a href="https://framacloud.org/fr/" class="btn btn-link">
                    <b class="violet">Frama</b><b class="vert">cloud</b>
                </a>
                <a href="https://framagit.org/framasoft/framacloud/" class="btn btn-link pull-right">
                    <i class="fa fa-fw fa-lg fa-git" aria-hidden="true"></i>
                    <span class="sr-only">git</span>
                </a>
            </td>
            <td>
                pour accompagner les <b>jardinier⋅ère⋅s</b><br>
                qui souhaitent installer/administrer les même services
            </td>
        </tr>
        <tr>
            <td>
                <i class="fa fa-fw fa-lg fa-paw" aria-hidden="true"></i>&nbsp;
                <a href="https://participer.framasoft.org/fr/" class="btn btn-link">
                    <b class="orange">Participer</b>
                </a>
                <a href="https://framagit.org/framasoft/participer/" class="btn btn-link pull-right">
                    <i class="fa fa-fw fa-lg fa-git" aria-hidden="true"></i>
                    <span class="sr-only">git</span>
                </a>
            </td>
            <td>
                pour accompagner les <b>contributeur⋅rice⋅s</b><br>
                bénévoles (ici même ;) )
            </td>
        </tr>
    </tbody>
</table>