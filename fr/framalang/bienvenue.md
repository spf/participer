# Bienvenue chez Framalang&nbsp;!

L'organisation de Framalang est très souple, flexible et réactive, une haute adaptabilité qui rend difficile l'écriture de «&nbsp;règles&nbsp;» de travail. Néanmoins, il faut avoir connaissance de quelques façons de faire pour démarrer correctement. Les voici&nbsp;!

Du [choix des textes à traduire jusqu'à leur publication sur le blog](chemin.html), trois outils sont principalement utilisés&nbsp;:
    
 - la [liste Framalang](framalang@framalistes.org),
 - les framapads pour la traduction proprement dite,
 - wordpress pour la publication.
    
>  **important**&nbsp;: pour bien comprendre les étapes d'une traduction, vous pouvez [lire cette page](chemin.html).

## la Framaliste

C'est l'outil de communication entre nous.

Elle sert, entre autres&nbsp;:
    
 - pour proposer des traductions en envoyant le lien vers le texte d'origine et quelques mots expliquant pourquoi le texte te parait intéressant. Si on reçoit deux ou trois réponses favorables, alors on continue. 
 - à informer les autres traducteur·trice·s si on a l'autorisation de traduction dans le cas d'un texte placé sous une licence ne permettant pas l'adaptation
 - à donner aux traducteur·trice·s l'adresse du framapad où a été copié le texte à traduire
 - à informer de l'état d'avancée de la traduction&nbsp;:
   - traduction finie, prête à relire
   - relecture finie, prête à bloguifier
   - parution sur le blog faite
   - toutes les étapes ne sont pas systématiquement renseignées, ça dépend des cas… 
 - à relancer une traduction délaissée
 - à signaler des coquilles dans le texte mis en ligne
 - et on pourrait continuer encore longtemps.
    
> **important**&nbsp;: L'objet du courriel commencera automatiquement par «&nbsp;[framalang]&nbsp;». C'est la configuration de la liste qui le fait, et cela permet de filtrer facilement les messages. Vous pouvez commencer vos propositions de traduction en ajoutant `[Proposition]` au début de votre objet, et annoncer les traductions par `[Traduction]`.


## le Framapad

### Création

Plusieurs volontaires se sont manifestés pour traduire le texte, il faut maintenant passer à l'étape suivante&nbsp;: la création d'un pad de traduction. Le plus souvent (et le plus pratique) c'est que celui qui a proposé la traduction ouvre le pad.

Pour créer un pad, et pour utiliser au mieux framapad, le mieux c'est un petit tour sur le [tutoriel Framapad](https://docs.framasoft.org/fr/etherpad/index.html)

Framalang dispose d'[un dossier MyPads partagé](https://mypads.framapad.org/mypads/?/mypads/group/framalang-7l3ibkl0/view). 

Pour avoir accès à ce dossier, il suffit de&nbsp;:
 - le demander via la liste Framalang
 - d'avoir un compte «&nbsp;[MyPads](https://mypads.framapad.org)&nbsp;»

C'est plus facile de retrouver les pads en cours via ce dossier partagé et c'est la solution à privilégier. Néanmoins, ce n'est pas indispensable et il est tout à fait possible d'ouvrir un framapad standard pour la traduction.

Une fois le pad créé, et le texte original en anglais copié dedans, son adresse est diffusée aux traducteurs via la liste.

> **important**&nbsp;:
> - En haut du pad, il faut toujours rappeler le lien vers l'article original (et éventuellement le lien vers cette page). 
> - le texte est à coller en mode texte, sans balise de formatage, sans mise en forme du texte, sans image. Toutefois, cela peut toujours être adapté à la situation.
> - Pensez à vous inspirer des autres pads de traduction pour créer le votre. Vous pouvez, par exemple, créer un espace "Le coin des trads" pour qu'on se note les usages, termes récurrents et questionnements liés à cette traduction.
 
### Traduction

> **important**&nbsp;: La traduction en français se fait paragraphe par paragraphe SOUS le paragraphe en anglais.

Les membres de Framalang ne sont pas des traducteur·trice·s professionnel·le·s. Ce qui permet de fournir une traduction de qualité c'est le travail collaboratif, c'est-à-dire que le texte va être traduit puis relu par plusieurs personnes. L'objectif est que chacun améliore le texte par passes successives. Dans ce cadre, il est normal de revenir sur une partie de traduction déjà écrite que ce soit pour corriger une faute de français, améliorer une tournure de phrase, faire une proposition alternative voire corriger un contresens dans la traduction.

Il ne faut donc pas hésiter à corriger le texte&nbsp;: c'est la base du travail collaboratif. 

Pour les corrections, l'usage est le suivant&nbsp;:
 - les fautes de français (orthographe, accords, etc.) sont à corriger directement
 - si vous avez une proposition alternative pour un mot, une portion de phrase voire une phrase, elle est écrite après le texte original, comme ça `/*texte alternatif*/` `/*autre texte alternatif*/`. Si l'ensemble du paragraphe est concerné, réécrire le texte en-dessous de la première traduction. Si une alternative vous semble pertinente vous pouvez l'indiquer par un `(+1)`
 - si vous estimez qu'une traduction ne respecte pas le texte original, barrez la partie concernée comme ça et réécrivez à la suite


En cas de doute, de question (ce qui arrive fréquemment), on peut&nbsp;:
 - en discuter en direct sur le chat, en bas à droite du pad
 - consulter les ressources d'aide à la traduction disponibles (voir ci-dessous)

> N'hésitez pas à utiliser (et à partager avec le groupe) certains [outils bien utiles pour les traductions](outils.html).

### relecture

Les textes sont en général relus sur le pad avant la bloguification. Lors de la relecture, on fait le choix entre les différentes propositions de traduction, plus la relecture est fine plus le travail du bloguificateur est allégé. Dans l'idéal, la relecture est faite par une personne n'ayant pas participé à la traduction.

C'est la personne qui met en ligne sur le blog (= qui bloguifie)qui a logiquement la décision finale sur la traduction quand il reste des choix multiples.

> **important**&nbsp;: il faut toujours veiller à rester dans le texte d'origine et à ne pas sur-traduire ou dénaturer un titre ou un propos. C'est un peu le risque des corrections successives, un mot en appelle un autre et au final, il peut arriver que l'on trouve un titre ou une tournure qui sonne bien, qui accroche le lecteur mais qui s'est finalement trop éloigné(e) du texte. Vigilance, toujours.

## Wordpress

Wordpress est le support du Framablog.

Cette partie est sans doute la plus normée, puisqu'elle aboutit à une sortie du texte du milieu restreint de Framalang pour devenir public.

> Tout est écrit ou presque dans ce [tuto](blog.html)

