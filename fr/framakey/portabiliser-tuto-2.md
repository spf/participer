# Rappel

## Discrétion

La « discrétion » d’un logiciel, c’est sa capacité à ne pas laisser de traces.
Ainsi, beaucoup de logiciels écrivent des données dans
`c:\Documents and Settings\Pierre\Applications Data\Logiciel`
(où « Pierre » est votre nom d’utilisateur, et où « Logiciel » est le nom du soft).
Plus tordu encore : certains logiciels, écrivent dans la « base de registre » de windows.

« La base de registre est une base de données utilisée par le système
d’exploitation Windows. Elle contient les données de configuration du
système d’exploitation et des autres logiciels installés désirant s’en servir.
Microsoft utilise aujourd’hui plutôt le terme Registre Windows pour
parler de cette base de données. » (merci [Wikipédia](http://fr.wikipedia.org/wiki/Base_de_registre) ;) )

Évidemment, si votre logiciel écrit des données « importantes » dans le
dossier `Documents and Settings` (qu’on va appeler `Docs&Settings`, si vous n’y voyez pas d’inconvénients)
ou dans la base de registre (qu’on va appeler `BDR`, si vous n’y voyez
pas d’inconvénients), et que vous voulez utiliser le logiciel sur une
autre machine, il va râler parce qu’il ne retrouvera pas les bonnes informations.

## Portabilité

La portabilité désigne pour un programme informatique sa capacité à
fonctionner plus ou moins facilement dans différents environnements
d’exécution. (merci qui ? [toujours Wikipédia](http://fr.wikipedia.org/wiki/Portabilit%C3%A9_%28informatique%29))

On notera ici que Framakey fait un abus de langage : nous devrions
plutôt parler de « logiciel nomade » plutôt que de « logiciel portable ».
Mais en fait la définition convient partiellement (sur son plus petit
dénominateur commun : « capacité à fonctionner sur différentes machines sous windows »),
et comme nous l’employons depuis plusieurs années, le terme est resté…

Ainsi, on pourra considérer qu’un logiciel est portable si on peut
déplacer le dossier du logiciel, le placer sur un autre support
(clé usb, autre machine, etc) et l’exécuter sans avoir à l’installer à nouveau.

## Framakey : portable et discret ?

Dans une situation idéale, les logiciels seraient parfaitement portables et discrets.
Malheureusement, on est pas dans le monde des bisounours, et ce cas est extrêmement rare.
Il faut donc savoir où placer la barre entre le « zéro défaut » et « l’importance du service rendu ».

Ainsi, certains logiciels nécessitent parfois des fichiers .dll fournis
d’office avec Windows pour fonctionner (par exemple MSVCR71.dll, qui se
trouve dans `c:\windows\system32`).
Si un logiciel portable utilise un fichier de windows, peut-on le
considérer comme portable ? A priori oui. Le souci, c’est que d’anciennes versions
de windows (Windows 95, par exemple) ne dispose pas nécessairement de
tels fichiers, le logiciel risque donc de ne pas être portable sur un tel système.

De même, certains logiciels écrivent des données « non sensibles »
dans la base de registre (par exemple en conservant la position de la
fenêtre lors de la dernière fermeture, ou la liaison entre le logiciel
et l’extension par défaut des fichiers que ce dernier utilise).
Doit-on alors le considérer comme non discret et ne pas le portabiliser ?

La règle, totalement arbitraire et évolutive (mais totalement assumée), que nous avons fixée, est la suivante :

 *  si un logiciel portable ne fonctionne pas sous les versions fraîchement
    installées des deux dernières versions de windows, il ne peut être considéré comme portable.
 *  si un logiciel portable laisse dans `Docs&Settings` ou dans la `BDR`
    des informations de nature personnelle (nom de l’utilisateur, noms
    des derniers fichiers créés/ouverts par l’utilisateur, heure
    d’utilisation, etc), il ne peut être considéré comme portable.

Ainsi, si un logiciel conserve des informations de configuration non
vitales sur le poste client, nous ne nous interdisons pas de l’ajouter
au portail d’applications portables Framakey.

A l’heure de rédaction de ce tutoriel, Windows Vista est le dernier
système d’exploitation Windows sorti, et XP l’avant dernier.
Si un logiciel portable fonctionne sur ces deux systèmes, il pourra
être ajouté. Que les utilisateurs de Windows 98 ou 2000 se rassurent :
nous avons commencé la création d’applications portables bien avant l
a sortie de Vista, donc beaucoup de nos logiciels sont compatibles sur
ces anciens systèmes (et inversement, certains n’ont pas encore été
testés sous Vista, car produit avant la sortie de ce système).

Prêts pour les tests de discrétion ? [Allons-y…](portabiliser-tuto-3.html)  
[Retourner à la page précédente…](portabiliser-tuto-1.html)
